package exo_methodes;

import java.io.File;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		
		File myFile_dir = new File("/Users/valer/Desktop/divers.txt");
		
		if(myFile_dir.exists()) {
			System.out.println("Le fichier existe\n");
			System.out.println(filesize_in_megaBytes(myFile_dir));
			System.out.println(filesize_in_Bytes(myFile_dir));
			System.out.println(filesize_in_kiloBytes(myFile_dir));
		}else {
			System.out.println("Le fichier n'existe pas!\n");
		}
		
		if(myFile_dir.isDirectory()) {
			System.out.println("C'est un répertoire\n");
		}else {
			System.out.println("Ce n'est pas un répertoire!");
		}
		
		if(myFile_dir.isFile()) {
			System.out.println("C'est un fichier\n");
		}else {
			System.out.println("Ce n'est pas un fichier!");
		}
		
		System.out.println("Le chemin absolu est " + myFile_dir.getAbsolutePath());
			
		
	}
	
	private static String filesize_in_megaBytes(File file) {
		return (double) file.length() / (1024*1024) + "mb";
	}
	private static String filesize_in_kiloBytes(File file) {
		return (double) file.length() / 1024 + "kb";
	}
	private static String filesize_in_Bytes(File file) {
		return (double) file.length() + "bytes";
	}

}
