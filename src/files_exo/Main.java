package files_exo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		
//		File file = new File("fichier.txt");
//		FileWriter filewriter = new FileWriter(file);
		// on peut fusionner les lignes
//		FileWriter filewriter = new FileWriter("fichier.txt",true);
//		
//		filewriter.write("Hello world!\n");
//		filewriter.write(86 + "\n");
//		filewriter.write("a\n");
//		filewriter.close();
		
		BufferedWriter bw = new BufferedWriter(new FileWriter("fichier3.txt"));
		bw.write("Salut la Terre!\n");
		bw.write(1999 + "\n");
		bw.close();
	}

}
