package files_lecture;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		
//		File file = new File("fichier.txt");
//		FileReader	fr = null;
//		try {
//			fr  = new FileReader(file);
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		int str = fr.read();
//		
//		while(str != -1) {
//			System.out.println((char) str);
//			str = fr.read();
//		}

		BufferedReader br = new BufferedReader(new FileReader("fichier.txt"));
		String string = br.readLine();
		
		while(string != null) {
			System.out.println(string);
			string = br.readLine();

		}
	}

}
